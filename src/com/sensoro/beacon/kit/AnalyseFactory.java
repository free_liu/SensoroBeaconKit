package com.sensoro.beacon.kit;

import java.util.UUID;

class AnalyseFactory {
	static final int protocolVersionLatest = 1;
	static final byte TYPE_UUID = 0;
	static final byte TYPE_MAJOR = 1;
	static final byte TYPE_MINOR = 2;
	static final byte TYPE_TX_POWER = 3;
	static final byte TYPE_ADV_INT = 4;
	
	static final byte CMD_R_CFG = 0;
	static final byte CMD_W_CFG = 1;
	static final byte CMD_W_PASSWORD = 2;
	static final byte CMD_LED = 3;
	static final byte CMD_RET_FAC = 4;

	static final byte ERROR_CODE_SUCCESS = 0;
	static final byte ERROR_CODE_NO_PERMISSION = 1;
	static final byte ERROR_CODE_INCAILD_PARAM = 2;
	static final byte ERROR_CODE_INVALID_LENGTH = 3;
	static final byte ERROR_CODE_INVALID_CFG_TYPE = 4;
	static final byte ERROR_CODE_INVALID_PASSWORD = 5;
	static final byte ERROR_CODE_INVALID_PASSWORD_LENGTH = 6;
	static final byte ERROR_CODE_CMD_TYPE = 7;
	static final byte ERROR_CODE_LED_BUSY = 8;
	static final byte ERROR_CODE_NONSUPPOTR = 9;
	
	static class GattInfo {
		public static final UUID CMD_SERVICE_UUID = UUID.fromString("DEAE0200-7A4E-1BA2-834A-50A30CCAE0E4");
		public static final UUID CMD_WRITE_UUID = UUID.fromString("DEAE0201-7A4E-1BA2-834A-50A30CCAE0E4");
		public static final UUID CMD_READ_UUID = UUID.fromString("DEAE0202-7A4E-1BA2-834A-50A30CCAE0E4");

		public static final UUID CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

	}

	static IAnalyse creatAnalyse(int ver, String hw, String fw) {
		switch (ver) {
		case protocolVersionLatest:
			if (hw.equals(Beacon.FV_30) && fw.equals(Beacon.HV_C0)) {
				return new Analyse_0_1(protocolVersionLatest);
			}
			return null;
		default:
			break;
		}
		return null;
	}
}
