package com.sensoro.beacon.kit;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Sensoro on 12/18/14.
 */
class Region implements Parcelable, Cloneable{
    static final int TYPE_U = 0;
    static final int TYPE_UM = 1;
    static final int TYPE_UMM = 2;
    static final int TYPE_UNKNOWN = 3;

    String identifier;
    String proximityUUID;
    int major;
    int minor;
    int type;
    ArrayList<Beacon> beacons = new ArrayList<Beacon>();

    public Region(){
        identifier = "";
        this.proximityUUID = "";
        this.major = Integer.MAX_VALUE;
        this.minor = Integer.MAX_VALUE;
        this.type = TYPE_UNKNOWN;
    }

    public Region(String identifier,String proximityUUID){
        this.identifier = identifier;
        this.proximityUUID = proximityUUID;
        this.major = Integer.MAX_VALUE;
        this.minor = Integer.MAX_VALUE;
        this.type = TYPE_U;
    }
    public Region(String identifier,String proximityUUID,int major){
        this.identifier = identifier;
        this.proximityUUID = proximityUUID;
        this.major = major;
        this.minor = Integer.MAX_VALUE;
        this.type = TYPE_UM;
    }
    public Region(String identifier,String proximityUUID,int major,int minor){
        this.identifier = identifier;
        this.proximityUUID = proximityUUID;
        this.major = major;
        this.minor = minor;
        this.type = TYPE_UMM;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(identifier);
        out.writeString(proximityUUID);
        out.writeInt(major);
        out.writeInt(minor);
        out.writeInt(type);
        for (Beacon beacon : beacons) {
            out.writeParcelable(beacon, flags);
        }
    }

    public static final Creator<Region> CREATOR
            = new Creator<Region>() {
        public Region createFromParcel(Parcel in) {
            return new Region(in);
        }

        public Region[] newArray(int size) {
            return new Region[size];
        }
    };

    private Region(Parcel in) {
        identifier = in.readString();
        proximityUUID = in.readString();
        major = in.readInt();
        minor = in.readInt();
        type = in.readInt();
    }

    public String getIdentifier(){
        return identifier;
    }

    public ArrayList<Beacon> getBeaconsInRegion(){
        return beacons;
    }

    public Region clone() throws CloneNotSupportedException {
        Region newRegion = null;
        try {
            newRegion = (Region) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return newRegion;
    }

    @Override
    public boolean equals(Object that) {
        if (!(that instanceof Region)) {
            return false;
        }
        Region thatRegion = (Region) that;
        return (thatRegion.identifier.equals(this.identifier));
    }
}
