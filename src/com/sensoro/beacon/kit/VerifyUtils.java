package com.sensoro.beacon.kit;

public class VerifyUtils {
	/**
	 * 校验 UUID 合法性
	 * 
	 * @param uuid
	 * @return
	 */
	static boolean checkUUIDLegal(String uuid) {
		// 长度不满足
		if (uuid.length() != BaseSettings.UUID_STRING_LENGTH) {
			return false;
		}
		// 中间第 7,14,19,24 位不为 '-'
		if (uuid.charAt(8) != '-' && uuid.charAt(13) != '-' && uuid.charAt(18) != '-' && uuid.charAt(23) != '-') {
			return false;
		}

		// 校验是否包含非 '0~f' 和 '-' 以外的字符
		uuid = uuid.toLowerCase(); // 字母全部转换小写
		for (int i = 0; i < uuid.length(); i++) {
			if (uuid.charAt(i) >= 'a' && uuid.charAt(i) <= 'f' || uuid.charAt(i) <= '9' && uuid.charAt(i) >= '0' || uuid.charAt(i) == '-') {
				continue;
			} else {
				return false;
			}
		}

		return true;
	}

	/**
	 * 校验 major 和 minor 合法性
	 * 
	 * @param major
	 * @param minor
	 * @return
	 */
	static boolean checkMajorMinorLegal(int majorOrMinor) {
		if (majorOrMinor > BaseSettings.MAX_65535 || majorOrMinor < BaseSettings.MIN_0) {
			return false;
		}
		return true;
	}
}
