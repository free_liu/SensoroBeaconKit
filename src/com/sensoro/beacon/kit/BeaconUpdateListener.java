/*
 * Copyright (c) 2014. Sensoro Inc.
 * All rights reserved.
 */

package com.sensoro.beacon.kit;


import java.util.ArrayList;

/**
 * Created by Sensoro on 14-6-23.
 */
interface BeaconUpdateListener {
    public void didBeacons(ArrayList<Beacon> beacons);
}
