package com.sensoro.beacon.kit;

import java.util.ArrayList;
import java.util.UUID;

public interface IAnalyse {

	byte[] combineReadCmd();

	BeaconConfiguration analysisCmd(byte[] data);

	ArrayList<byte[]> combineDataPackages(BeaconConfiguration config);
}
