package com.sensoro.beacon.kit;

import android.os.Parcelable;

import com.sensoro.beacon.kit.Beacon.MovingState;

/**
 * Created by Sensoro on 15/1/26.
 */
interface BeaconListener extends Parcelable{
    public void onUpdateRssi(int rssi);
    public void onUpdateTemperature(Integer temperature);
    public void onUpdateLight(Double light);
    public void onUpdateMovingState(MovingState movingState);
    public void onUpdateAccelerometerCount(int count);
}
