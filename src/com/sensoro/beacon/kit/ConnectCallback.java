package com.sensoro.beacon.kit;

/**
 * Created by Sensoro on 15/1/15.
 */
abstract interface ConnectCallback {
    /**
     * The connected status.
     */
    public static final int STATE_CONNECTED = 0;
    /**
     * The disconnected status.
     */
    public static final int STATE_DISCONNECTED = 1;
    /**
     * Success.
     */
    public static final int SUCCESS = 0;
    /**
     * Failure.
     */
    public static final int FAILURE = 1;
//    /**
//     * Setting data invalid.
//     */
//    public static final int SETTING_DATA_INVALID = 2;
//    /**
//     * Write not permitted.
//     */
//    public static final int WRITE_NOT_PERMITTED = 3;
//    /**
//     * Authorization failed.
//     */
//    public static final int AUTHORIZATION_FAILED = 4;
    /**
     * Connect beacon timeout.
     */
    public static final int CONNECTED_TIME_OUT = 5;
    /**
     * Beacon model error.
     */
    public static final int ERROR_BEACON_MODEL = 6;
    /**
     * The method is not support the current version of Yunzi.
     */
    public static final int NOT_SUPPORT = 7;

    public void onConnectedState(int newState, int status);
}
