package com.sensoro.beacon.kit;

/**
 * Created by Sensoro on 15/1/15.
 */
interface WriteCallback {
    public abstract void onSuccess();
    public abstract void onFailure(int errorCode);
}
