package com.sensoro.beacon.kit;

import android.app.IntentService;
import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by Sensoro on 12/18/14.
 */
public class IntentProcessorService extends IntentService {
    private MonitoredRegion monitoredRegion;
    private MonitoredRegion updateRegion;
    private MonitoredBeacon monitoredBeacon;
    private ArrayList<Beacon> updateBeacons;

    public IntentProcessorService() {
        super("IntentProcessor");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            monitoredBeacon = (MonitoredBeacon) intent.getExtras().get(BeaconProcessService.MONITORED_BEACON);
            monitoredRegion = (MonitoredRegion) intent.getExtras().get(BeaconProcessService.MONITORED_REGION);
            updateRegion = (MonitoredRegion) intent.getExtras().get(BeaconProcessService.UPDATE_BEACONS_IN_REGION);
            updateBeacons = (ArrayList<Beacon>) intent.getExtras().get(BeaconProcessService.UPDATE_BEACONS);
        }

        if (monitoredBeacon != null){
            BeaconManagerListener beaconManagerListener = SensoroBeaconManager.getInstance(getApplication()).getBeaconManagerListener();
            if (beaconManagerListener != null){
                if (monitoredBeacon.inSide){
                    beaconManagerListener.onNewBeacon(monitoredBeacon.beacon);
                } else {
                    beaconManagerListener.onGoneBeacon(monitoredBeacon.beacon);
                }
            }
        }
        if (monitoredRegion != null){

        }
        if (updateRegion != null){

        }
        if (updateBeacons != null){
            BeaconManagerListener beaconManagerListener = SensoroBeaconManager.getInstance(getApplication()).getBeaconManagerListener();
            if (beaconManagerListener != null){
                beaconManagerListener.onUpdateBeacon(updateBeacons);
            }
        }
    }
}
