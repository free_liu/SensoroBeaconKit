package com.sensoro.beacon.kit;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sensoro on 12/18/14.
 */
class MonitoredRegion implements Parcelable{
    Region region;
    boolean inSide;

    public MonitoredRegion(Region region, boolean inSide) {
        this.region = region;
        this.inSide = inSide;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(region, flags);
        out.writeByte((byte) (inSide ? 1 : 0));
    }

    public static final Creator<MonitoredRegion> CREATOR
            = new Creator<MonitoredRegion>() {
        public MonitoredRegion createFromParcel(Parcel in) {
            return new MonitoredRegion(in);
        }

        public MonitoredRegion[] newArray(int size) {
            return new MonitoredRegion[size];
        }
    };

    private MonitoredRegion(Parcel in) {
        region = in.readParcelable(this.getClass().getClassLoader());
        inSide = in.readByte() == 1;
    }
}
