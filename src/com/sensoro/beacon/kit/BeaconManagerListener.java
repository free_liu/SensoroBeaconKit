package com.sensoro.beacon.kit;

/**
 * Created by Sensoro on 1/7/15.
 */

import java.util.ArrayList;

/**
 * The callback interface when beacons appear, disappear, update.
 */
public interface BeaconManagerListener {
    /**
     * The callback method when a beacon appears.
     *
     * @param beacon the beacon that appears.
     */
    public void onNewBeacon(Beacon beacon);

    /**
     * The callback method when a beacon disappears.
     *
     * @param beacon the beacon that disappears.
     */
    public void onGoneBeacon(Beacon beacon);

    /**
     * This callback method will be called every assigned period.The period
     * is assigned by
     * {@link com.sensoro.beacon.kit.SensoroBeaconManager#setUpdateBeaconPeriod(long)}
     *
     * @param beacons current beacon list
     */
    public void onUpdateBeacon(ArrayList<Beacon> beacons);
}
