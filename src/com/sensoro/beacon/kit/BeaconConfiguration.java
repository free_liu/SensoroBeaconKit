package com.sensoro.beacon.kit;

import java.util.HashMap;

/**
 * Created by Sensoro on 15/5/22.
 */
public class BeaconConfiguration {
	public static String UUID_ILLEGAL_EXCEPTION = "uuid illegal";
	public static String MAJOR_ILLEGAL_EXCEPTION = "major illegal";
	public static String MINOR_ILLEGAL_EXCEPTION = "minor illegal";

	HashMap<Integer, Object> backupDatas;
	public static int[] integerType = { AnalyseFactory.TYPE_MAJOR, AnalyseFactory.TYPE_MINOR };
	public static int[] stringType = { AnalyseFactory.TYPE_UUID };

	String uuid = null;
	Integer major = null;
	Integer minor = null;
	BaseSettings.TransmitPower transmitPower = BaseSettings.TransmitPower.UNKNOWN;
	BaseSettings.AdvertisingInterval advertisingInterval = BaseSettings.AdvertisingInterval.UNKNOWN;
	BaseSettings.EnergySavingMode energySavingMode = BaseSettings.EnergySavingMode.UNKNOWN;
	boolean isIBeaconEnable = true;
	String password = null;
	String broadcastKey = null;
	Integer temperatureSamplingInterval = null;
	Integer lightSamplingInterval = null;
	SensorSettings.AccelerometerSensitivity accelerometerSensitivity = SensorSettings.AccelerometerSensitivity.UNKNOWN;
	boolean isAliBeaconEnable = false;

	private BeaconConfiguration(Builder builder) throws IllegalAccessException {
		backupDatas = new HashMap<>();
		uuid = builder.uuid;
		if (uuid != null) {
			if (VerifyUtils.checkUUIDLegal(uuid)) {
				backupDatas.put((int) AnalyseFactory.TYPE_UUID, uuid);
			} else {
				throw new IllegalAccessException(UUID_ILLEGAL_EXCEPTION);
			}
		}
		major = builder.major;
		if (major != null) {
			if (VerifyUtils.checkMajorMinorLegal(major)) {
				backupDatas.put((int) AnalyseFactory.TYPE_MAJOR, major);
			} else {
				throw new IllegalAccessException(MAJOR_ILLEGAL_EXCEPTION);
			}
		}
		minor = builder.minor;
		if (minor != null) {
			if (VerifyUtils.checkMajorMinorLegal(minor)) {
				backupDatas.put((int) AnalyseFactory.TYPE_MINOR, minor);
			} else {
				throw new IllegalAccessException(MINOR_ILLEGAL_EXCEPTION);
			}
		}
		transmitPower = builder.transmitPower;
		advertisingInterval = builder.advertisingInterval;
		energySavingMode = builder.energySavingMode;
		isIBeaconEnable = builder.isIBeaconEnable;
		password = builder.password;
		broadcastKey = builder.broadcastKey;
		temperatureSamplingInterval = builder.temperatureSamplingInterval;
		lightSamplingInterval = builder.lightSamplingInterval;
		accelerometerSensitivity = builder.accelerometerSensitivity;
		isAliBeaconEnable = builder.isAliBeaconEnable;

	}

	public static class Builder {
		private String uuid = null;
		private Integer major = null;
		private Integer minor = null;
		private BaseSettings.TransmitPower transmitPower = BaseSettings.TransmitPower.UNKNOWN;
		private BaseSettings.AdvertisingInterval advertisingInterval = BaseSettings.AdvertisingInterval.UNKNOWN;
		private BaseSettings.EnergySavingMode energySavingMode = BaseSettings.EnergySavingMode.UNKNOWN;
		private boolean isIBeaconEnable = true;
		private String password = null;
		private String broadcastKey = null;
		private Integer temperatureSamplingInterval = null;
		private Integer lightSamplingInterval = null;
		private SensorSettings.AccelerometerSensitivity accelerometerSensitivity = SensorSettings.AccelerometerSensitivity.UNKNOWN;
		private boolean isAliBeaconEnable = false;

		public Builder setUuid(String uuid) throws IllegalAccessException {
			if (!VerifyUtils.checkUUIDLegal(uuid)) {
				throw new IllegalAccessException(UUID_ILLEGAL_EXCEPTION);
			}
			this.uuid = uuid;
			return this;
		}

		public Builder setMajor(int major) throws IllegalAccessException {
			if (!VerifyUtils.checkMajorMinorLegal(major)) {
				throw new IllegalAccessException(MAJOR_ILLEGAL_EXCEPTION);
			}
			this.major = major;
			return this;
		}

		public Builder setMinor(int minor) throws IllegalAccessException {
			if (!VerifyUtils.checkMajorMinorLegal(minor)) {
				throw new IllegalAccessException(MINOR_ILLEGAL_EXCEPTION);
			}
			this.minor = minor;
			return this;
		}

		public Builder setTransmitPower(BaseSettings.TransmitPower transmitPower) {
			this.transmitPower = transmitPower;
			return this;
		}

		public Builder setAdvertisingInterval(BaseSettings.AdvertisingInterval advertisingInterval) {
			this.advertisingInterval = advertisingInterval;
			return this;
		}

		public Builder setEnergySavingMode(BaseSettings.EnergySavingMode energySavingMode) {
			this.energySavingMode = energySavingMode;
			return this;
		}

		public Builder enableIBeacon(boolean isIBeaconEnable) {
			this.isIBeaconEnable = isIBeaconEnable;
			return this;
		}

		public Builder setPassword(String password) {
			this.password = password;
			return this;
		}

		public Builder setBroadcastKey(String broadcastKey) {
			this.broadcastKey = broadcastKey;
			return this;
		}

		public Builder setTemperatureSamplingInterval(int temperatureSamplingInterval) {
			this.temperatureSamplingInterval = temperatureSamplingInterval;
			return this;
		}

		public Builder setLightSamplingInterval(int lightSamplingInterval) {
			this.lightSamplingInterval = lightSamplingInterval;
			return this;
		}

		public Builder setAccelerometerSensitivity(SensorSettings.AccelerometerSensitivity accelerometerSensitivity) {
			this.accelerometerSensitivity = accelerometerSensitivity;
			return this;
		}

		public BeaconConfiguration creat() throws Exception {
			return new BeaconConfiguration(this);
		}
	}

	String getUuid() {
		return uuid;
	}

	void setUuid(String uuid) {
		if (uuid != null) {
			backupDatas.put((int) AnalyseFactory.TYPE_UUID, uuid);
		}
		this.uuid = uuid;
	}

	Integer getMajor() {
		return major;
	}

	void setMajor(Integer major) {
		if (major != null) {
			backupDatas.put((int) AnalyseFactory.TYPE_MAJOR, major);
		}
		this.major = major;
	}

	Integer getMinor() {
		return minor;
	}

	void setMinor(Integer minor) {
		if (minor != null) {
			backupDatas.put((int) AnalyseFactory.TYPE_MINOR, minor);
		}
		this.minor = minor;
	}

	BaseSettings.TransmitPower getTransmitPower() {
		return transmitPower;
	}

	void setTransmitPower(BaseSettings.TransmitPower transmitPower) {
		this.transmitPower = transmitPower;
	}

	BaseSettings.AdvertisingInterval getAdvertisingInterval() {
		return advertisingInterval;
	}

	void setAdvertisingInterval(BaseSettings.AdvertisingInterval advertisingInterval) {
		this.advertisingInterval = advertisingInterval;
	}

	BaseSettings.EnergySavingMode getEnergySavingMode() {
		return energySavingMode;
	}

	void setEnergySavingMode(BaseSettings.EnergySavingMode energySavingMode) {
		this.energySavingMode = energySavingMode;
	}

	boolean isIBeaconEnable() {
		return isIBeaconEnable;
	}

	void setIBeaconEnable(boolean isIBeaconEnable) {
		this.isIBeaconEnable = isIBeaconEnable;
	}

	String getPassword() {
		return password;
	}

	void setPassword(String password) {
		this.password = password;
	}

	String getBroadcastKey() {
		return broadcastKey;
	}

	void setBroadcastKey(String broadcastKey) {
		this.broadcastKey = broadcastKey;
	}

	Integer getTemperatureSamplingInterval() {
		return temperatureSamplingInterval;
	}

	void setTemperatureSamplingInterval(Integer temperatureSamplingInterval) {
		this.temperatureSamplingInterval = temperatureSamplingInterval;
	}

	Integer getLightSamplingInterval() {
		return lightSamplingInterval;
	}

	void setLightSamplingInterval(Integer lightSamplingInterval) {
		this.lightSamplingInterval = lightSamplingInterval;
	}

	SensorSettings.AccelerometerSensitivity getAccelerometerSensitivity() {
		return accelerometerSensitivity;
	}

	void setAccelerometerSensitivity(SensorSettings.AccelerometerSensitivity accelerometerSensitivity) {
		this.accelerometerSensitivity = accelerometerSensitivity;
	}

	boolean isAliBeaconEnable() {
		return isAliBeaconEnable;
	}

	void setAliBeaconEnable(boolean isAliBeaconEnable) {
		this.isAliBeaconEnable = isAliBeaconEnable;
	}
}
