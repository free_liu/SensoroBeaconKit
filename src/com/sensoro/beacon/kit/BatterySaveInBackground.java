/*
 * Copyright (c) 2014. Sensoro Inc.
 * All rights reserved.
 */

package com.sensoro.beacon.kit;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

/**
 * If you use the feature, the sdk will save power obviously,but it will affect
 * the performance of the sdk.
 */
class BatterySaveInBackground implements Application.ActivityLifecycleCallbacks {
    private SensoroBeaconManager sensoroBeaconManager;
    private int activeActivityCount;

    public BatterySaveInBackground(Context context) {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return;
        }
        if (context instanceof Application ) {
            ((Application)context).registerActivityLifecycleCallbacks(this);
        }
        sensoroBeaconManager = SensoroBeaconManager.getInstance(context);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        activeActivityCount++;
        if (activeActivityCount < 1) {
            activeActivityCount = 1;
        }
        sensoroBeaconManager.setBackgroundMode(false);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        activeActivityCount--;
        if (activeActivityCount < 1) {
            sensoroBeaconManager.setBackgroundMode(true);
        }
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    	
    }
}
