package com.sensoro.beacon.kit;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sensoro on 12/18/14.
 */
class MonitoredBeacon implements Parcelable{
    Beacon beacon;
    boolean inSide;

    public MonitoredBeacon(Beacon beacon, boolean inSide) {
        this.beacon = beacon;
        this.inSide = inSide;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(beacon, flags);
        out.writeByte((byte) (inSide ? 1 : 0));
    }

    public static final Parcelable.Creator<MonitoredBeacon> CREATOR
            = new Parcelable.Creator<MonitoredBeacon>() {
        public MonitoredBeacon createFromParcel(Parcel in) {
            return new MonitoredBeacon(in);
        }

        public MonitoredBeacon[] newArray(int size) {
            return new MonitoredBeacon[size];
        }
    };

    private MonitoredBeacon(Parcel in) {
        beacon = in.readParcelable(this.getClass().getClassLoader());
        inSide = in.readByte() == 1;
    }
}
