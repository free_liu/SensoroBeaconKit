/*
 * Copyright (c) 2014. Sensoro Inc.
 * All rights reserved.
 */

package com.sensoro.beacon.kit;

class BeaconEvent {
    public Beacon beacon;
    public long time;

    BeaconEvent(Beacon beacon, long time){
        this.beacon = beacon;
        this.time = time;
    }
}
